
// Envoie les valeurs du capteur au format OSC sur le réseau à l'adresse 192.168.1.10 sur le port 8003
// Adresse OSC de destination : /from/arduino/1

#include <RunningAverage.h>
#include <Ethernet.h>  //pour la première version de shield Ethernet
#include <EthernetUdp.h> //pour la première version de shield Ethernet
//#include <Ethernet2.h> //pour la deuxième version de shield Ethernet
//#include <EthernetUdp2.h> //pour la deuxième version de shield Ethernet
#include <SPI.h>
#include <OSCMessage.h>
#include <math.h>

RunningAverage runAvgMinute(60);
RunningAverage runAvg(10);
const int sampleWindow = 100; // Sample window width in mS (50 mS = 20Hz)
unsigned int sample;
unsigned int audioLevel = 0;   // peak-to-peak level

EthernetUDP Udp;


//the Arduino's IP au college
//IPAddress ip(172, 23, 48, 71); //#1 => CDI
//IPAddress ip(172, 23, 48, 72); //#2 => salle de cours 9
//IPAddress ip(172, 23, 48, 73); //#3 => couloir_télévision
//IPAddress ip(172, 23, 48, 74); //#4 => salle musique
//IPAddress ip(172, 23, 48, 75); //#5 => salle art plastique
//IPAddress ip(172, 23, 48, 76); //#6 => polyvalante
//IPAddress ip(172, 23, 48, 77); //#7 => petite salle info
IPAddress ip(172, 23, 48, 78); //#8 =>grande salle info (26)
//IPAddress ip(172, 23, 48, 79); //#9 =>salle des profs

//destination IP au college
IPAddress outIp(172, 23, 48, 70);//server (raspberry)
//IPAddress outIp(172, 23, 48, 133);//server (mon ordi)

//the Arduino's IP a l'atelier
//IPAddress ip(192, 168, 0, 11); //#1 =>
//IPAddress ip(192, 168, 0, 12); //#2 =>
//IPAddress ip(192, 168, 0, 13); //#3 =>
//IPAddress ip(192, 168, 0, 14); //#4 =>
//IPAddress ip(192, 168, 0, 15); //#5 =>
//IPAddress ip(192, 168, 0, 16); //#6 =>
//IPAddress ip(192, 168, 0, 17); //#7 =>
//IPAddress ip(192, 168, 0, 18); //#8 =>
//IPAddress ip(192, 168, 0, 19); //#9 =>
//IPAddress ip(192, 168, 0, 20); //#10 =>

//destination IP a l'atelier
//IPAddress outIp(192, 168, 0, 2);//server (raspberry)

const unsigned int outPort = 8004;

byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
}; // you can find this written on the board of some Arduino Ethernets or shields

void setup() {
  Serial.begin(115200);
  Ethernet.begin(mac,ip);
  Udp.begin(8888);
  runAvgMinute.clear();
  runAvg.clear(); // explicitly start clean
}

void loop() {
  //AUDIO LEVEL
  unsigned long startMillis = millis(); // Start of sample window
  unsigned int signalMax = 0;
  unsigned int signalMin = 1024;
  // collect data for 50 mS
  while (millis() - startMillis < sampleWindow)
  {
    sample = analogRead(0);
    if (sample < 1024)  // toss out spurious readings
    {
      if (sample > signalMax)
      {
        signalMax = sample;  // save just the max levels
      }
      else if (sample < signalMin)
      {
        signalMin = sample;  // save just the min levels
      }
    }
  }
  audioLevel = signalMax - signalMin;  // max - min = peak-peak amplitude
  double volts = (audioLevel * 5.0) / 1024;  // convert to volts

  Serial.print("  audioLevel: ");
  Serial.print(audioLevel);

  //AVERAGE
  long runAvgAudioLevel = audioLevel;
  runAvgMinute.addValue(runAvgAudioLevel);
  runAvg.addValue(runAvgAudioLevel);
  Serial.print("  runAvgMinute: ");
  Serial.print(runAvgMinute.getAverage(), 3);

  //AUTO-CALIBRATION
  int NormaudioLevel = ((audioLevel) - (runAvg.getMin())) / ((runAvg.getMax()) - (runAvg.getMin())) * (100 - 0);
  Serial.print("  Norm: ");
  //Serial.println(NormaudioLevel);

  //int normLog = 20*log(NormaudioLevel);
  //Serial.println(normLog); 

  //MESSAGE OSC
  //the message wants an OSC address as first argument
  OSCMessage msg("/from/arduino/8");
  msg.add(audioLevel);
  msg.add(runAvgMinute.getAverage());
  msg.add(NormaudioLevel);
  //msg.add(normLog);

  Udp.beginPacket(outIp, outPort);
  msg.send(Udp); // send the bytes to the SLIP stream
  Udp.endPacket(); // mark the end of the OSC Packet
  msg.empty(); // free space occupied by message
}
