// Envoie les valeurs du capteur au format OSC sur le réseau à l'adresse 192.168.1.10 sur le port 8003
// Adresse OSC de destination : /from/arduino/1

#include <RunningAverage.h>
#include <Ethernet2.h>
#include <EthernetUdp2.h>
#include <SPI.h>
#include <OSCMessage.h>
#include <math.h>

RunningAverage runAvgMinute(60);
RunningAverage runAvg(10);
unsigned int audioLevel = 0;
EthernetUDP Udp;


//the Arduino's IP au college
//IPAddress ip(172, 23, 48, 171); //#1 =>
//IPAddress ip(172, 23, 48, 172); //#2 =>
//IPAddress ip(172, 23, 48, 173); //#3 =>
//IPAddress ip(172, 23, 48, 174); //#4 =>
//IPAddress ip(172, 23, 48, 175); //#5 =>
//IPAddress ip(172, 23, 48, 176); //#6 =>
//IPAddress ip(172, 23, 48, 177); //#7 =>
//IPAddress ip(172, 23, 48, 178); //#8 =>
//IPAddress ip(172, 23, 48, 179); //#9 =>
//IPAddress ip(172, 23, 48, 180); //#10 =>
//destination IP au college
//IPAddress outIp(172, 23, 48, 130);//server (raspberry)
//IPAddress outIp(172, 23, 48, 133);//server (mon ordi)

//the Arduino's IP a l'atelier
IPAddress ip(192, 168, 0, 11); //#1 =>
//IPAddress ip(192, 168, 0, 12); //#2 =>
//IPAddress ip(192, 168, 0, 13); //#3 =>
//IPAddress ip(192, 168, 0, 14); //#4 =>
//IPAddress ip(192, 168, 0, 15); //#5 =>
//IPAddress ip(192, 168, 0, 16); //#6 =>
//IPAddress ip(192, 168, 0, 17); //#7 =>
//IPAddress ip(192, 168, 0, 18); //#8 =>
//IPAddress ip(192, 168, 0, 19); //#9 =>
//IPAddress ip(192, 168, 0, 20); //#10 =>

//destination IP a l'atelier
IPAddress outIp(192, 168, 0, 2);//server (raspberry)

const unsigned int outPort = 8004;



byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
}; // you can find this written on the board of some Arduino Ethernets or shields

void setup() {
  Serial.begin(115200);
  Ethernet.begin(mac,ip);
  Udp.begin(8888);
  runAvgMinute.clear();
  runAvg.clear(); // explicitly start clean
}

void loop() {

  // read the input on analog pin 0:
  audioLevel = analogRead(A0);
  // print out the value you read:
  
  Serial.print("  audioLevel: ");
  Serial.print(audioLevel);
  delay(1);        // delay in between reads for stability
  //AVERAGE
  long runAvgAudioLevel = audioLevel;
  runAvgMinute.addValue(runAvgAudioLevel);
  runAvg.addValue(runAvgAudioLevel);
  Serial.print("  runAvgMinute: ");
  Serial.print(runAvgMinute.getAverage(), 4);

  //AUTO-CALIBRATION
  int NormaudioLevel = ((audioLevel) - (runAvg.getMin())) / ((runAvg.getMax()) - (runAvg.getMin())) * (100 - 0);
  Serial.print("  Norm: ");
  //Serial.println(NormaudioLevel);

  //int normLog = 20*log(NormaudioLevel);
  //Serial.println(normLog); 

  //MESSAGE OSC
  //the message wants an OSC address as first argument
  OSCMessage msg("/from/arduino/1");
  msg.add(audioLevel);
  msg.add(runAvgMinute.getAverage());
  msg.add(NormaudioLevel);
  //msg.add(normLog);

  Udp.beginPacket(outIp, outPort);
  msg.send(Udp); // send the bytes to the SLIP stream
  Udp.endPacket(); // mark the end of the OSC Packet
  msg.empty(); // free space occupied by message
}
