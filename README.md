# ROSA-PARKS / EXSITU

## Installation sonore au collège Rosa Parks par ExSitu        
Années 2021-2022 - Dispositif "Plasticiens au collège"      


### Configuration réseau actuelle

Raspberry Pi :      
Nom : pi        
IP fixe (ethernet) : 192.168.1.10       

### Se connecter en ssh au Raspberry
ssh pi@192.168.1.10
Password :      

### Arduino Hardware
Connexion hardware du capteur d'amplitude sonore au Arduino:         
**capteur      =>      arduino**              
out          =>      A0     
gnd          =>      gnd        
vcc          =>      3.3V       

### Code Arduino
    
```
// Envoie les valeurs du capteur au format OSC sur le réseau à l'adresse 192.168.1.10 sur le port 8003
// Adresse OSC de destination : /from/arduino/1    
#include <Ethernet2.h>
#include <EthernetUdp2.h>
#include <SPI.h>    
#include <OSCMessage.h>

EthernetUDP Udp;

//the Arduino's IP
IPAddress ip(192, 168, 1, 33);
//destination IP
IPAddress outIp(192, 168, 1, 10);
const unsigned int outPort = 8003;

 byte mac[] = {  
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED }; // you can find this written on the board of some Arduino Ethernets or shields

const int sampleWindow = 100; // Sample window width in mS (50 mS = 20Hz)
unsigned int sample;

void setup() {
  Serial.begin(9600);
  Ethernet.begin(mac,ip);
  Udp.begin(8888);
}

void loop(){
  unsigned long startMillis= millis();  // Start of sample window
   unsigned int peakToPeak = 0;   // peak-to-peak level
   unsigned int signalMax = 0;
   unsigned int signalMin = 1024;
   // collect data for 50 mS
   while (millis() - startMillis < sampleWindow)
   {
      sample = analogRead(0);
      if (sample < 1024)  // toss out spurious readings
      {
         if (sample > signalMax)
         {
            signalMax = sample;  // save just the max levels
         }
         else if (sample < signalMin)
         {
            signalMin = sample;  // save just the min levels
         }
      }
   }
   peakToPeak = signalMax - signalMin;  // max - min = peak-peak amplitude
   double volts = (peakToPeak * 5.0) / 1024;  // convert to volts
   Serial.println(peakToPeak);
   
  //the message wants an OSC address as first argument
  OSCMessage msg("/from/arduino/1");
  msg.add(peakToPeak);
  
  Udp.beginPacket(outIp, outPort);
  msg.send(Udp); // send the bytes to the SLIP stream
  Udp.endPacket(); // mark the end of the OSC Packet
  msg.empty(); // free space occupied by message
}
```

### Accéder depuis l’extérieur au serveur Raspberry

**Redirection des port pour accéder au Raspberry**      
https://storj-labs.gitbook.io/node/dependencies/port-forwarding     
https://raspberry-pi.fr/mettre-en-ligne-serveur-web-raspbian-dydns-port-forwarding/ (server apache2)        

**Se connecter sur l'IP de la box dans un navigateur :**     exemple:  192.168.1.1

Aller dans  l'onglet configuration des ports : exemple: http://192.168.1.1/network/nat       
Et ouvrir un nouveau port qui doit ressembler à ça:     
Nom:HTTP | Protocole:TCP | Type:Port | Ports externe:80 | IP de destination:192.168.1.10 | Ports de destination:80   

**DynDNS - renommage de l'IP en URL**       
https://storj-labs.gitbook.io/node/dependencies/port-forwarding (voir : Setup Dynamic DNS Service: Dynamic Update Client Tool)

**Service de renommage des IP :**       
https://my.noip.com/dynamic-dns 

Sur le site de my-noip.com, créer un nouvel hostname

1. choisir un "hostname", exemple : exbox-test
2. choisir un nom de domaine, exemple : hopto.org
3. sélectionner "DNS Host (A)"
4. entrer l'IP public, celle de la box (pour connaitre son IP public : https://www.monippublique.com/)

Coté box, aller dans l'onglet DynDNS : 
sur la page http://192.168.1.1/network/ddns
1. selectionner le service "no-ip.com
2. entrer le nom d'utilisteur de no-ip.com
3. entrer le mot de passe de no-ip.com
4. enfin, entrer le nom de domaine : exbox-test.hopto.org

Coté Raspberry, il faut installer, configurer et démarrer "noip"        
https://storj-labs.gitbook.io/node/dependencies/port-forwarding     
https://unix.stackexchange.com/questions/199178/run-automatically-noip2-when-the-machine-is-booted#218771       

Il faut rediriger le port 80 (box) au port 8000 (port défini au lancement d'OSC)
`sudo iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 8000`

Suivi ce tuto pour figer, lors du démarrage, le routing de port : https://askubuntu.com/questions/270693/how-can-set-these-iptables-rules-to-run-at-startup     
"You may want to use the iptables-persistent package rather than mess with your boot scripts.  First, run your script to set up the firewall rules.  Secondly, run sudo apt-get install iptables-persistent, and follow the prompts.  When it asks to save the current rules, hit "Yes" at both prompts.  Now, on reboots, your iptables rules will be restored."       

Il y a eu un problème avec dhclient eth0, il fallait à chaque reboot faire un "sudo dhclient eth0" pour que ça fonctionne. Problème résolu en  ajoutant à la fin de `/etc/network/interfaces` :
```
auto lo
iface lo inet loopback
```
et décomenter :
```
auto eth0
NetworkManager#iface eth0 inet dhcp
```

Si on ne peut pas se connecter via internet, lancer la commande:
`sudo dhclient`

### Open Stage Control (node.js)

Script de lancement de node au démarage:        
`sudo nano /etc/systemd/system/nodeserver.service`      
copier:
```
UMask=007
[Service]
ExecStart=/usr/bin/node /home/pi/rosa-parks/osc/server/open-stage-control-server.js -l /home/pi/rosa-parks/osc/server/test1.json -p 8000 -s localhost:8002 -o 8003
Restart=always
StandardOutput=/var/log/open-stage-control.log
StandardError=/var/log/open-stage-control.log
User=pi
# Configures the time to wait before service is stopped forcefully.
TimeoutStopSec=300
[Install]
WantedBy=multi-user.target
```

### PureData     
Script de lancement de PureData au démarage:        
`sudo nano /etc/systemd/system/puredata.service`   
copier:     
```
UMask=007
[Service]
ExecStart=/usr/bin/sudo /usr/bin/pd -nogui -nrt -nosound -open /home/pi/rosa-parks/puredata-test/pd-osc.pd
Restart=always
StandardOutput=/var/log/puredata.log
StandardError=/var/log/puredata.log
User=pi
# Configures the time to wait before service is stopped forcefully.
TimeoutStopSec=300
[Install]
WantedBy=multi-user.target
```
### Redémarage automatique
Programmer le raspberry pour rebooter tout les jours à 0HOO     
https://www.windtopik.fr/redemarrage-automatique-raspberry-pi/



